from rich.console import Console
console = Console()

input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day2_pt1.txt", "r")

input = [game.split(" ") for game in input_file.read().split("\n")][0:-1]

def score(o, m):
    win = 6
    draw = 3
    loss = 0

    choice = {'X': 1, 'Y': 2, 'Z': 3}
    results = {
        'X': {'A': draw, 'B': loss, 'C': win}, # rock draw v rock, loss v paper, win v scissors
        'Y': {'A': win, 'B': draw, 'C': loss}, # paper win v rock, draw v paper, loss v scissors
        'Z': {'A': loss, 'B': win, 'C': draw} # scissors loss v rock, win v paper, draw v scissors
    }

    return choice[m] + results[m][o]

scores = [score(opponent, me) for [opponent, me] in input]

console.print(f"Score: {sum(scores)}")
