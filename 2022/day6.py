from rich.console import Console
console = Console()
import re

input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day6.txt", "r")
# inputs = [
#     'mjqjpqmgbljsphdztnvjfqwrcgsmlb',
#     'bvwbjplbgvbhsrlpgdmjqwftvncz',
#     'nppdvjthqldpwncqszvftbrmjlhg',
#     'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg',
#     'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw']
inputs = [input_file.read()]

for input_string in inputs:
    for i,x in enumerate(input_string):
        if i >= 3:
            string = input_string[i-3:i+1]

            if sum([len(re.findall(s, string)) for s in string]) <= 4:
                console.print(f"Packet starts at character {i+1}, string {string}")
                break

for input_string in inputs:
    for i,x in enumerate(input_string):
        if i >= 13:
            message = input_string[i-13:i+1]

            if sum([len(re.findall(s, message)) for s in message]) <= 14:
                console.print(f"Message starts at character {i+1}, message {message}")
                break
