from rich.console import Console
console = Console()
import re

# input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day10.txt", "r")
input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day10_sample.txt", "r")
# [console.print(command.split('\n')) for command in input_file.read().split('\n')]
inputs = [command.split('\n') for command in input_file.read().split('\n')][0:-1]
console.print(inputs)
