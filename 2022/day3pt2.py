from rich.console import Console
console = Console()

input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day3_pt1.txt", "r")
# input_file = ["vJrwpWtwJgWrhcsFMMfFFhFp",
#     "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
#     "PmmdzqPrVvPwwTWBwg",
#     "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
#     "ttgJtRGJQctTZtZT",
#     "CrZsJsPPZsGzwwsLwLmpwMDw"]

sacks = input_file.read().split("\n")[0:-1]

groups = [sacks[i:i + 3] for i in range(0, len(sacks), 3)]

common = [''.join(set.intersection(*map(set,group))) for group in groups]
console.print(common)
item_values = [''] + [chr(num) for num in range(ord('a'), ord('z') + 1)] + [chr(num) for num in range(ord('A'), ord('Z') + 1)]

values = [item_values.index(item) for item in common]
console.print(f"Total: {sum(values)}")
