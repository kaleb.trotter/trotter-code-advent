from rich.console import Console
console = Console()

input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day2_pt1.txt", "r")
# input_file = ["A Y",
# "B X",
# "C Z"]

input = [game.split(" ") for game in input_file.read().split("\n")][0:-1]
# input = [game.split(" ") for game in input_file]

def score(o, m):
    win = 6
    draw = 3
    loss = 0

    rock = 1
    paper = 2
    scissors = 3

    result = {'X': loss, 'Y': draw, 'Z': win}
    choice = {
        'X': {'A': scissors, 'B': rock, 'C': paper}, # losing against rock picks scissors, against paper picks rock, against scissors picks paper,
        'Y': {'A': rock, 'B': paper, 'C': scissors}, # draw picks the sequence
        'Z': {'A': paper, 'B': scissors, 'C': rock} # win against rock picks paper, against paper picks scissors, against scissors picks rock
    }

    return result[m] + choice[m][o]

scores = [score(opponent, me) for [opponent, me] in input]

console.print(f"Score: {sum(scores)}")
