from rich.console import Console
console = Console()

input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day3_pt1.txt", "r")
# input_file = ["vJrwpWtwJgWrhcsFMMfFFhFp",
#     "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
#     "PmmdzqPrVvPwwTWBwg",
#     "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
#     "ttgJtRGJQctTZtZT",
#     "CrZsJsPPZsGzwwsLwLmpwMDw"]

sacks = [[rucksack[:len(rucksack) // 2], rucksack[len(rucksack) // 2:]] for rucksack in input_file.read().split("\n")[0:-1]]
# sacks = [[rucksack[:len(rucksack) // 2], rucksack[len(rucksack) // 2:]] for rucksack in input_file]

common = [''.join(set(item1).intersection(item2)) for [item1,item2] in sacks]

item_values = [''] + [chr(num) for num in range(ord('a'), ord('z') + 1)] + [chr(num) for num in range(ord('A'), ord('Z') + 1)]

values = [item_values.index(item) for item in common]
console.print(f"Total: {sum(values)}")
