from rich.console import Console
console = Console()

input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day1_pt1.txt", "r")

input = [[int(cals) if cals != '' else 0 for cals in elf.split("\n")] for elf in input_file.read().split("\n\n")]

total_per_elf = [sum(elf_carries) for elf_carries in input]
console.print(f"Max: {max(total_per_elf)}")

order = sorted(total_per_elf)

console.print(f"Total of top three: {sum(order[-3::])}")
