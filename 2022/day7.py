from rich.console import Console
console = Console()
import re

# input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day7.txt", "r")
input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day7_sample.txt", "r")
[console.print(command.split('\n')) for command in input_file.read().split('$ ')]
inputs = [command.split('\n') for command in input_file.read().split('$ ')]
console.print(inputs)
