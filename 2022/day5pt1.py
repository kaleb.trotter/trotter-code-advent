from rich.console import Console
console = Console()
import re
import copy

input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day5.txt", "r")
# input_file = [
#     "    [D]    ",
#     "[N] [C]    ",
#     "[Z] [M] [P]",
#     " 1   2   3 ",
#     "",
#     "move 1 from 2 to 1",
#     "move 3 from 1 to 3",
#     "move 2 from 2 to 1",
#     "move 1 from 1 to 2",
# ]

input_file = input_file.read().split("\n")[0:-1]
split_point = input_file.index('')

start_map = [row for row in input_file[:split_point-1]]

columns = input_file[split_point-1:split_point][0]
column_array = [int(num) for num in columns.split(' ')[1::3]]

instructions = input_file[split_point+1:]

stacks = [re.sub("    ", " [ ]", row).replace(' ', '').replace('[]', '[ ]') for row in start_map]

stack_array = [[items.replace('[','').replace(']','') for items in re.findall('\[[A-Z\W]\]', row)] for row in stacks]
stack_array.reverse()

# Use zip to transpose
horiz_array = [list(x) for x in zip(*stack_array)]
horiz_array = [list(filter(lambda x: x != ' ', row)) for row in horiz_array]

inst_num_only = [re.findall('\d+', instruction) for instruction in instructions]
inst_obj = [{'num': int(num), 'source': int(source), 'dest': int(dest)} for num,source,dest in inst_num_only]

console.print(horiz_array)

final_9000 = copy.deepcopy(horiz_array)
for instruction in inst_obj:
    num,src,dest = instruction.values()

    for i in range(num):
        crate = final_9000[src - 1].pop()
        final_9000[dest - 1].append(crate)

final_9001 = copy.deepcopy(horiz_array)
for instruction in inst_obj:
    num,src,dest = instruction.values()

    crates = final_9001[src - 1][-num:]
    final_9001[src - 1] = final_9001[src - 1][:-num]
    final_9001[dest - 1] += crates

word_9000 = ''
for stack in final_9000:
    word_9000 += stack.pop() if len(stack) else ''
word_9001 = ''
for stack in final_9001:
    word_9001 += stack.pop() if len(stack) else ''

console.print(f"CrateMover 9000: {word_9000}")
console.print(f"CrateMover 9001: {word_9001}")
