from rich.console import Console
console = Console()

input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2022/input_day4.txt", "r")

pairs = [assmts.split(',') for assmts in input_file.read().split("\n")[0:-1]]

fully_overlap = []
partial_overlap = []
for [elf1, elf2] in pairs:
    start1,stop1 = [int(s) for s in elf1.split('-')]
    start2,stop2 = [int(s) for s in elf2.split('-')]

    if (start1 <= start2 and stop1 >= stop2) or (start1 >= start2 and stop1 <= stop2):
        fully_overlap += [[elf1, elf2]]

    if (start1 <= start2 and stop1 >= start2) or (start2 <= start1 and stop2 >= start1):
        partial_overlap += [[elf1, elf2]]

console.print(f"Contained: {len(fully_overlap)}")
console.print(f"Overlap: {len(partial_overlap)}")