inputs = [line.rstrip('\n') for line in open('day_two_inputs.txt')]
# inputs = ['abcde','fghij','klmno','pqrst','fguij','axcye','wvxyz', 'fjhjj', 'fonyqmjyqufrapeczikghtvdxl', 'fontsmeyqudrapeczikghtvdxl']

from collections import Counter
from itertools import combinations

# Part 1
# has_three = 0
# has_two = 0
# for boxid in inputs:
# 	counter = Counter(boxid)
# 	if 3 in counter.values():
# 		has_three += 1
# 	if 2 in counter.values():
# 		has_two += 1

# print(has_three * has_two)

# Part 2
inputs = set(inputs)


# for boxid in inputs:
# 	for anotherbox in inputs:
# 		z = zip(anotherbox, boxid)
# 		print([pair for pair in list(z)])
matches = []
for pair in list(combinations(inputs, 2)):
	i = 0
	for letterpair in list(zip(pair[0], pair[1])):
		if letterpair[0] != letterpair[1]:
			i += 1
	if i == 1:
		matches.append(pair)
print(matches)


	# [zip(anotherbox, inputs) for anotherbox in inputs if anotherbox != boxid]
	# for anotherbox in inputs:
	# 	if (boxid != anotherbox) and (len([x for x in boxid if x not in anotherbox]) == 1) and (len([x for x in anotherbox if x not in boxid]) == 1):
	# 		print([x for x in anotherbox if x not in boxid], [x for x in boxid if x not in anotherbox])
	# 		print(boxid, anotherbox)