inputs = [line.rstrip('\n') for line in open('day_three_inputs.txt')]
# inputs = ['#1 @ 1,3: 4x4', '#2 @ 3,1: 4x4', '#3 @ 5,5: 2x2']

import re
import numpy as np

z = np.zeros((1000,1000))

def parse_row(row):
	coords = [int(x) for x in row[1].split(',')]
	length = [int(x) for x in row[2].split('x')]
	return coords[0], coords[1], coords[0] + length[0], coords[1] + length[1]

# Part 1
for row in [[s.strip() for s in re.split('@ |: ', s)] for s in inputs]:
	start_x, start_y, end_x, end_y = parse_row(row)
	z[start_x:end_x, start_y:end_y] += 1

for row in [[s.strip() for s in re.split('@ |: ', s)] for s in inputs]:
	start_x, start_y, end_x, end_y = parse_row(row)
	if((z[start_x:end_x, start_y:end_y] > 1).sum() == 0):
		print(row[0])

print(np.count_nonzero(z>1))

# Part 2
