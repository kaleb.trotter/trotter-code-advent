inputs = [int(line.rstrip('\n')) for line in open('day_one_inputs.txt')]
# inputs = [-6, +3, +8, +5, -6]
# 
from itertools import accumulate, cycle

# Part 1
print(f'Sum: {sum(inputs)}')

# Part 2
freq = 0
seen = set([])
j = 0

# Better solution from reddit:
# print(next(f for f in accumulate(cycle(inputs)) if f in seen or seen.add(f)))

for f in cycle(inputs):
	if f == -76214:
		j += 1
	freq += f
	if freq in seen:
		print(f'Freq: {freq}')
		print(f'{j} loops')
		break
	seen.add(freq)
