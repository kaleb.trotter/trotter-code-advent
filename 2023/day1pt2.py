from rich.console import Console
import re
console = Console()

input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2023/input_day1_pt1.txt", "r")

input = input_file.read().split("\n")\

digits = [line.replace('one', 'o1e').replace('two', 't2o').replace('three', 't3e').replace('four', 'f4r').replace('five', 'f5e').replace('six', 's6x').replace('seven', 's7n').replace('eight', 'e8t').replace('nine', 'n9e').replace('zero', 'z0o') for line in input]
console.print(digits)
digit_pattern = re.compile("\d")
console.print(digits)
digits = [digit_pattern.findall(line) for line in digits]

# Processing step to turn written words into digits

numbers = [int(''.join([line[0], line[-1]])) for line in digits]

console.print(f"Total: {sum(numbers)}")
