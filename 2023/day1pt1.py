from rich.console import Console
import re
console = Console()

input_file = open("/Users/kaleb.trotter/Desktop/dev/trotter-code-advent/2023/input_day1_pt1.txt", "r")

pattern = re.compile("\d")

# input = [pattern.findall(line) for line in input_file.read().split("\n\n")]
input = [pattern.findall(line) for line in input_file.read().split("\n")]

numbers = [int(''.join([line[0], line[-1]])) for line in input]

console.print(f"Total: {sum(numbers)}")
